/**
 * Use this script tocalculate and inject additional CSS to the middle data-container to expand width to ~99vh.
 */

let windowWidth = window.innerWidth;
let domContents = document.querySelectorAll('div.main-container > div[class^=content-container]');
let currentTotalWidth = 0;
let newContentWidth = 0;

for(i = 0; i < domContents.length; i++) {
    currentTotalWidth += domContents[i].offsetWidth;
}

newContentWidth = windowWidth - domContents[0].offsetWidth - domContents[2].offsetWidth - 100;

domContents[1].setAttribute('style', 'width: ' + newContentWidth + 'px !important;');
