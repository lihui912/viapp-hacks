How to create bookmarklet
=========================
 1. Step 1: Minify the script using this tool: [https://xem.github.io/terser-online/](https://xem.github.io/terser-online/)
 2. Step 2: Create Bookmarklet using this tool: [https://mrcoles.com/bookmarklet/](https://mrcoles.com/bookmarklet/)
 3. Step 3: Copy the code snippet to the Minify tool, it will generate the monified code.
 4. Step 4: Copy the monified code from Step 3, paste it to the Bookmarklet tool.
 5. Step 5: Copy the Bookmarklet code from Step 4, create a new bookmark in browser, paste the copied code into the address field, name the bookmark.
 6. Step 4: Done.