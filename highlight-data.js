/**
 * Use this script to inject additional CSS to highlight key data in red color and bold them.
 */

let togglerIncomeStmt = document.querySelector('input#income-statement-title2.toggle');
let togglerCashFlowStmt = document.querySelector('input#cashflow-statement-title2.toggle');
let togglerBalanceSheetStmt = document.querySelector('input#balance-sheet-title2.toggle');
let togglerGrowthStmt = document.querySelector('input#growth-key-ratio-title2.toggle');

togglerIncomeStmt.checked = true;
togglerCashFlowStmt.checked = true;
togglerBalanceSheetStmt.checked = true;
togglerGrowthStmt.checked = true;

let highlightComponents = [];   // DOM components to be highlight

let tableIncomeStatement = document.querySelector('div#income-statement-key-ratio2.table-container');
let tableCashFlowStatement = document.querySelector('div#cashflow-statement-key-ratio2.table-container');
let tableBalanceSheetStatement = document.querySelector('div#balance-sheet-key-ratio2.table-container');
let tableGrowthStatement = document.querySelector('div#growth-key-ratio2.table-container');

let indexIncomeStmt = [
    2,  // P: Revenue
    7,  // P: Net Income (adj)
    23, // P: Diluted EPS (adj)
    13, // E: Return on Equity (Adj.)
];

let indexCashFlowStmt = [
    3,  // I: Cash From Operations
    4,  // I: CAPEX
    7,  // I: Free Cash Flow (FCF)
];

let indexBalanceSheetStmt = [
    10, // C: Total Debt to Equity Ratio
    2,  // C: Short Term Cash Investments
    4,  // C: Total Dept
];

let indexGrowthSheetStmt = [
    17, // Growth: Diluted (Adj.) EPS Growth: YoY
    18, // Growth: Diluted (Adj.) EPS Growth: 3 Year CAGR
];


let incomeStmtHcell = tableIncomeStatement.querySelectorAll('div.h-cell');
let incomeStmtScroller = tableIncomeStatement.querySelectorAll('div.scroller-row');
indexIncomeStmt.forEach((item, index) => {
    highlightComponents.push(incomeStmtHcell[item]);
    highlightComponents.push(incomeStmtScroller[item]);
});

let cashFlowStmtHcell = tableCashFlowStatement.querySelectorAll('div.h-cell');
let cashFlowStmtScroller = tableCashFlowStatement.querySelectorAll('div.scroller-row');
indexCashFlowStmt.forEach((item, index) => {
    highlightComponents.push(cashFlowStmtHcell[item]);
    highlightComponents.push(cashFlowStmtScroller[item]);
});

let balanceSheetStmtHcell = tableBalanceSheetStatement.querySelectorAll('div.h-cell');
let balanceSheetStmtScroller = tableBalanceSheetStatement.querySelectorAll('div.scroller-row');
indexBalanceSheetStmt.forEach((item, index) => {
    highlightComponents.push(balanceSheetStmtHcell[item]);
    highlightComponents.push(balanceSheetStmtScroller[item]);
});

let growthSheetStmtHcell = tableGrowthStatement.querySelectorAll('div.h-cell');
let growthSheetStmtScroller = tableGrowthStatement.querySelectorAll('div.scroller-row');
indexGrowthSheetStmt.forEach((item, index) => {
    highlightComponents.push(growthSheetStmtHcell[item]);
    highlightComponents.push(growthSheetStmtScroller[item]);
});

highlightComponents.forEach((item) => {
    item.style.fontWeight = 'bold';
    item.style.color = 'red';
});



